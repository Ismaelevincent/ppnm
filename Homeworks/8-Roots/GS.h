#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#ifndef HAVE_QR
void qr_dec  (gsl_matrix* A, gsl_matrix* R);
void qr_back  (gsl_matrix* A, gsl_vector* R);
void qr_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b,gsl_vector* qb);
void qr_inv  (gsl_matrix* Q, gsl_matrix* R, gsl_matrix *B);
#define HAVE_QR
#endif
