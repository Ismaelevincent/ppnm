#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<float.h>
#include<math.h>
#include<assert.h>

static double rmax;
static int ncalls;

int newton (void f(gsl_vector* x,gsl_vector* fx), gsl_vector* x, double eps);
double Fe(double e,double r);

void vector_print(char* s,gsl_vector* v)
{
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
	printf("\n");
}
void master(gsl_vector*x,gsl_vector*fx)
{
	double e=gsl_vector_get(x,0);
	assert(e<0);
	double frmax=Fe(e,rmax);
	gsl_vector_set(fx,0,frmax);
}

void f(gsl_vector* p,gsl_vector* fx)
{
	ncalls++;
	double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
	gsl_vector_set(fx,0, -2*(1-x)-400*(y-x*x)*x);//df/dx
	gsl_vector_set(fx,1, 200*(y-x*x));//df/dy
}

int main(int argc, char** argv) 
{
	if(argc>1)
	{
		rmax = atof(argv[1]);
		int dim=1;
		gsl_vector* x=gsl_vector_alloc(dim);
		gsl_vector_set(x,0,-1);
		newton(master,x,1e-3);
		double e=gsl_vector_get(x,0);

		fprintf(stderr,"\n__________\n Exercise B\n__________\n");
		fprintf(stderr,"Rmax=%g;\ncalculated energy=%g\nexact energy=-0.5 \n",rmax,e);
		

		printf("# r, Fe(e,r), exact\n");
		for(double r=0;r<=rmax;r+=rmax/64)
			printf("%g %g %g\n",r,Fe(e,r),r*exp(-r));
		printf("\n\n");
	}
	else
	{
		gsl_vector* x=gsl_vector_alloc(2);
		gsl_vector_set(x,0,-3);
		gsl_vector_set(x,1,4);
		gsl_vector* fx=gsl_vector_alloc(2);
		printf("\n__________\n Exercise A\n__________\n");
		printf("Root finding:\n");
		printf("extremum of the Rosenbrock's function:\n");
		vector_print("initial vector x: ",x);
		f(x,fx);
		vector_print("            f(x): ",fx);
		ncalls=0;
		int steps=newton(f,x,1e-3);
		printf("steps = %i, ncalls = %i\n",steps,ncalls);
		vector_print("      solution x: ",x);
		f(x,fx);
		vector_print("            f(x): ",fx);
	}
return 0;
}
