#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_deriv.h>
#include <gsl/gsl_integration.h>
#include"ann.h"

double integral(double x) {return -0.5*exp(-x*x);} 
double derivative(double x) {return exp(-x*x)*(1.-2.*x*x);} 
double activation_function(double x){return x*exp(-x*x);}
double function_to_fit(double x){return cos(5*x-1)*exp(-x*x);}
double fit (double x, void * params)
{
  (void)(params); /* avoid unused parameter warning */
  return function_to_fit(x);
}


int main()
{
	int n=4;
	ann* network=ann_alloc(n,activation_function);
	ann* integ=ann_alloc(n, integral);
	ann* deriv=ann_alloc(n, derivative);
	double a=-1,b=1;
	int nx=20;
	gsl_vector* vx=gsl_vector_alloc(nx);
	gsl_vector* vy=gsl_vector_alloc(nx);
	gsl_function F;
	double abserr;
  	F.function = &fit;
  	F.params = 0;
	for(int i=0;i<nx;i++)
	{
		double x=a+(b-a)*i/(nx-1);
		double f;
		f=function_to_fit(x);
		gsl_vector_set(vx,i,x);
		gsl_vector_set(vy,i,f);
	}

	for(int i=0;i<network->n;i++)
	{
		gsl_vector_set(network->params,3*i+0,a+(b-a)*i/(network->n-1));
		gsl_vector_set(network->params,3*i+1,1);
		gsl_vector_set(network->params,3*i+2,1);
	}

	ann_train(network,vx,vy);
	gsl_vector_memcpy(integ->params, network->params);
	gsl_vector_memcpy(deriv->params, network->params);
	for(int i=0;i<network->n;i++)
	{
		double bi=gsl_vector_get(network->params,3*i+1);
		double wi=gsl_vector_get(network->params,3*i+2);
		gsl_vector_set(deriv->params,3*i+2,wi/bi);
		gsl_vector_set(integ->params,3*i+2,wi*bi);
	}	
	printf("# x data\n");
	for(int i=0;i<vx->size;i++)
	{
		double x=gsl_vector_get(vx,i);
		double f=gsl_vector_get(vy,i);
		printf("%g %g\n",x,f);
	}
	printf("\n\n");
	printf("\n# x fit ann_derivative gsl_derivative ann_integral gsl_integral\n");
	double dz=1.0/64;
	for(double z=a;z<=b;z+=dz)
	{	double int_gsl, der_gsl;
		double y=ann_response(network,z);
		double y_int=ann_response(integ,z)+fabs(ann_response(integ,-1));
		double y_der=ann_response(deriv,z);
		gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);
		gsl_integration_qags (&F, a, z, 0, 1e-7, 1000, w, &int_gsl, &abserr);
		gsl_integration_workspace_free (w);	
		gsl_deriv_central(&F, z, 1e-8, &der_gsl, &abserr);
		printf("%g %g %g %g %g %g\n", z, y, y_der, der_gsl, y_int, int_gsl);
	}
	
	for(int i=0;i<network->n;i++)
	{
		double ai=gsl_vector_get(network->params,3*i+0);
		double bi=gsl_vector_get(network->params,3*i+1);
		double wi=gsl_vector_get(network->params,3*i+2);
		fprintf(stderr,"i=%i ai,bi,wi = %g %g %g\n",i,ai,bi,wi);
	}
	fprintf(stderr, "\n derivative \n");
	for(int i=0;i<network->n;i++)
	{
		double ai=gsl_vector_get(deriv->params,3*i+0);
		double bi=gsl_vector_get(deriv->params,3*i+1);
		double wi=gsl_vector_get(deriv->params,3*i+2);
		fprintf(stderr,"i=%i ai,bi,wi = %g %g %g\n",i,ai,bi,wi);
	}
gsl_vector_free(vx);
gsl_vector_free(vy);
ann_free(network);
ann_free(integ);
ann_free(deriv);

return 0;
}
