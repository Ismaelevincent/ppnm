#include<math.h>
#include<assert.h>
#include<stdio.h>
#include"adapt.h"
#define SQR2 1.41421356237309504880

int CC=0; //defines if it should use the Clenshaw Curtis function or not

static double A,B; // not accessible from other files
double F(double f(double),double t)
{ 
	return f( (A+B)/2+(B-A)/2*cos(t) )*sin(t)*(B-A)/2;
}

double open4 (double f(double),double a, double b, double acc, double eps, double f2, double f3, int limit, int CC)
{
	double f1,f4;
	if(CC==1) f1=F(f,a+(b-a)/6), f4=F(f,a+5*(b-a)/6);
	else f1=f(a+(b-a)/6), f4=f(a+5*(b-a)/6);
	double Q=(2*f1+f2+f3+2*f4)/6*(b-a), q=(f1+f4+f2+f3)/4*(b-a);
	double tolerance=acc+eps*fabs(Q), error=fabs(Q-q)/3;
	if(limit==0){
		fprintf(stderr,"open4: recursion limit reached\n");
		return Q;
		}
	if(error < tolerance) return Q;
	else {
		double Q1=open4(f,a,(a+b)/2,acc/SQR2,eps,f1,f2,limit-1, CC);
		double Q2=open4(f,(a+b)/2,b,acc/SQR2,eps,f3,f4,limit-1, CC);
		return Q1+Q2; }
}

double adapt(double f(double),double a,double b,double acc,double eps)
{
	CC=0;
	double f2=f(a+2*(b-a)/6),f3=f(a+4*(b-a)/6);
	int limit=99;
	return open4(f,a,b,acc,eps,f2,f3,limit, CC);
}

double clenshaw_curtis(double f(double),double a,double b, double acc,double eps )
{
	CC=1;
	A=a; B=b;
	a=0; b=M_PI;
	double f2=F(f,a+2*(b-a)/6),f3=F(f,a+4*(b-a)/6);
	int limit=99;
	return open4(f,a,b,acc,eps,f2,f3,limit, CC);
}

