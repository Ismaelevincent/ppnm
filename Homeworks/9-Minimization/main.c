#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
int qnewton(double cost(gsl_vector*x), gsl_vector*x, double acc);
void numeric_gradient(double cost(gsl_vector*x),gsl_vector*x, gsl_vector*g);

void vector_print(const char* s,gsl_vector* v)
{
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%9.4g ",gsl_vector_get(v,i));
	printf("\n");
}

double rosen(gsl_vector*v)
{
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow(1-x,2)+100*pow(y-x*x,2);
}

double himmel(gsl_vector*v)
{
	double x=gsl_vector_get(v,0);
	double y=gsl_vector_get(v,1);
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}

int N=30; 
double *E,*S,*D;
double BW(double A,double m,double G,double e)
{
	return A/(pow(e-m,2) + G*G/4 );
}
double dev(gsl_vector* v)
{
	double sum=0;
	double m=gsl_vector_get(v,0);
	double G=gsl_vector_get(v,1);
	double A=gsl_vector_get(v,2);
	for(int i=0;i<N;i++)
		sum+=pow(BW(A,m,G,E[i])-S[i],2)/pow(D[i],2);
	return sum;
}


int main()
{
	int n=2,nsteps;
	gsl_vector* x=gsl_vector_alloc(n);
	gsl_vector* exact=gsl_vector_alloc(n);
	gsl_vector* g=gsl_vector_alloc(n);
	double acc=1e-4;
	
	printf("\n_______________\nExercise A\n_______________\n");

	printf("\nMINIMIZATION OF ROSENBROCK'S FUNCTION\n");
	gsl_vector_set(x,0,18);
	gsl_vector_set(x,1,15);
	gsl_vector_set(exact,0,1);
	gsl_vector_set(exact,1,1);
	vector_print("start point   :",x);
	printf      ("value(start)  : %g\n",rosen(x));
	nsteps=qnewton(rosen,x,acc);
	vector_print("minimum found :",x);
	vector_print("exact minimum :",exact);
	printf      ("steps         : %i\n",nsteps);
	printf      ("value(min)    : %g\n",rosen(x));
	numeric_gradient(rosen,x,g);
	printf      ("acc           : %.1e\n",acc);
	vector_print("gradient(min) :",g);

	printf("\nMINIMIZATION OF HIMMELBLAU'S FUNCTION\n");
	gsl_vector_set(x,0,-3);
	gsl_vector_set(x,1,4);
	gsl_vector_set(exact,0,-2.805);
	gsl_vector_set(exact,1,-3.131);
	vector_print("start point   :",x);
	printf      ("value(start)  : %g\n",himmel(x));
	nsteps=qnewton(himmel,x,acc);
	printf      ("steps         : %i\n",nsteps);
	vector_print("minimum       :",x);
	vector_print("exact minimum :",exact);
	printf      ("value(min)    : %g\n",himmel(x));
	numeric_gradient(himmel,x,g);
	printf      ("acc           : %.1e\n",acc);
	vector_print("gradient(min) :",g);
	
	printf("\n_______________\nExercise B\n_______________\n");
	
	printf("\nMINIMIZATION OF THE DEVIATION OF BW FUNCTION (mass, gamma, sqrt(dev))\n");
	E=(double*)malloc(N*sizeof(double));
	S=(double*)malloc(N*sizeof(double));
	D=(double*)malloc(N*sizeof(double));
	n=3;
	gsl_vector* x1=gsl_vector_alloc(n);
	//gsl_vector* exact1=gsl_vector_alloc(n);
	gsl_vector* g1=gsl_vector_alloc(n);
	double ei, si, di;
	int items, i=0;
	FILE* input = fopen("data.txt", "r");
	do {
		items = fscanf(input,"%lg %lg %lg", &ei, &si, &di); //from stdin
//		printf("\n %i %g %g %g", items, ei, si, di); 
		E[i]=ei;
		S[i]=si;
		D[i]=di;
		i++;
	}while(items!=EOF);

//	for(int i=0; i<N; i++)
//	printf("\n %g %g %g", E[i], S[i], D[i]); 




	gsl_vector_set(x1,0,122);
	gsl_vector_set(x1,1,0.01);
	gsl_vector_set(x1,2,0.01);
	
	gsl_vector_set(exact,0,-2.805);
	gsl_vector_set(exact,1,-3.131);
	vector_print("\nstart point   :",x1);
	printf      ("value(start)  : %g\n",himmel(x1));
	nsteps=qnewton(dev,x1,acc);
	printf      ("steps         : %i\n",nsteps);
	vector_print("minimum       :",x1);
	vector_print("exact minimum :",exact);
	printf      ("value(min)    : %g\n",himmel(x1));
	numeric_gradient(himmel,x,g);
	printf      ("acc           : %.1e\n",acc);
	vector_print("gradient(min) :",g1);
	free(E);
	free(S);
	free(D);	
	
return 0;
}
