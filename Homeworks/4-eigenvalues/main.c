#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#define RND ((double)rand()/RAND_MAX-0.5)*2

#define FMT "%7.3f"
void printm(gsl_matrix* A)
{
	for(int n=0;n<A->size1;n++)
	{
		for(int m=0;m<A->size2;m++) printf(FMT,gsl_matrix_get(A,n,m));
		printf("\n");
	}
}

int jacobi_diag(gsl_matrix* A,gsl_matrix* V);

int main(int argc, char** argv)
{
	size_t n=4;
	if(argc>1)n=atoi(argv[1]);
	gsl_matrix* A =gsl_matrix_alloc(n,n);
	gsl_matrix* V =gsl_matrix_alloc(n,n);
	gsl_matrix_set_identity(V);
	for(int i=0;i<n;i++)
		for(int j=i;j<n;j++)
		{
			double aij=RND;
			gsl_matrix_set(A,i,j,aij);
			gsl_matrix_set(A,j,i,aij);
		}
	gsl_matrix* AA=gsl_matrix_alloc(n,n);
	gsl_matrix* X =gsl_matrix_alloc(n,n);
	gsl_matrix* Y =gsl_matrix_alloc(n,n);
	gsl_matrix_memcpy(AA,A);
	printf("\n__________\n Exercise A\n__________\n");
	printf("a random real symmetric matrix A:\n");
	printm(A);
	jacobi_diag(A,V);
	printf("matrix D (A after jacobi_diag, should be diagonal):\n");
	printm(A);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,AA,0,X);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,X,V,0,Y);
	printf("matrix V^T*A*V (should equal A after jacobi_diag):\n");
	printm(Y);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,V,A,0,X);
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,X,V,0,Y);
	printf("matrix V*D*V^T (should equal original A):\n");
	printm(Y);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,V,0,X);
	printf("matrix V^T*V (should equal identity matrix):\n");
	printm(X);
	printf("\n__________\n Exercise B\n__________\n");
	return 0;
}
