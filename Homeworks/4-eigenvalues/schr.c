#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#define RND ((double)rand()/RAND_MHX-0.5)*2

#define FMT "%7.3f"
void printm(gsl_matrix* A)
{
	for(int n=0;n<5;n++)
	{
		for(int m=0;m<5;m++) printf(FMT,gsl_matrix_get(A,n,m));
		printf("\n");
	}
}

int jacobi_diag(gsl_matrix* H,gsl_matrix* V);

int main(int argc, char** argv)
{
	size_t n=200;
	if(argc>1)n=atoi(argv[1]);
	gsl_matrix* H =gsl_matrix_calloc(n,n);
	gsl_matrix* V =gsl_matrix_alloc(n,n);
	gsl_matrix_set_identity(V);
	double s=1./n;
	double s2=s*s;
	double hii=2./s2;
	double hij=-1./s2;
	for(int i=0;i<n;i++)
	{
		gsl_matrix_set(H,i,i,hii);
		if(i>0) gsl_matrix_set(H,i,i-1,hij);
		if(i<n-1) gsl_matrix_set(H,i,i+1,hij);
	}


	gsl_matrix* HH=gsl_matrix_alloc(n,n);
	gsl_matrix* X =gsl_matrix_alloc(n,n);
	gsl_matrix* Y =gsl_matrix_alloc(n,n);
	gsl_matrix_memcpy(HH,H);
	printf("Matrix H:\n");
	printm(H);
	
	jacobi_diag(H,V);

	printf("matrix H after jacobi (first 5 rows and columns):\n");
	printm(H);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,HH,0,X);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,X,V,0,Y);
	printf("matrix V^T*H*V (first 5 rows and columns):\n");
	printm(Y);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,V,H,0,X);
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,X,V,0,Y);
	printf("matrix V*D*V^T (first 5 rows and columns):\n");
	printm(Y);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,V,0,X);
	printf("matrix V^T*V (first 5 rows and columns):\n");
	printm(X);

	fprintf(stderr,"#k calculated exact\n");
	for (int k=0; k < n/3; k++)
	{
		double exact = M_PI*M_PI*(k+1)*(k+1);
	    	double calculated = gsl_matrix_get(H,k,k);
		fprintf(stderr,"%i %g %g\n",k,calculated,exact);
	}
	
	printf("\n\n#index 1: First 3 eigenfunctions: \n 0 0 0 0 \n");
 	for(int i=0;i<n;i++) 
		printf("%g %g %g %g\n",(i+1.0)/(n+1), gsl_matrix_get(V,i,0), gsl_matrix_get(V,i,1), gsl_matrix_get(V,i,2));		;
  	printf("1 0 0 0\n");  	
}
