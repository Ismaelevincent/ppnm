#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include<assert.h>

void qr_dec(gsl_matrix* A, gsl_matrix* R)
{
	assert(A->size1>=A->size2); //checks n>=m
	int m=A->size2;
	for(int i=0; i<m; i++)
	{
		gsl_vector_view a = gsl_matrix_column(A,i); 
		double n = gsl_blas_dnrm2(&a.vector); //norm
		gsl_matrix_set(R,i,i,n);
		gsl_vector_scale(&a.vector, 1/n); 
		for(int j=i+1;j<m;j++) //it goes column by column
		{
			gsl_vector_view q = gsl_matrix_column(A,j);
			double s=0; gsl_blas_ddot(&a.vector,&q.vector,&s);
			gsl_blas_daxpy(-s,&a.vector,&q.vector); 
			gsl_matrix_set(R,i,j,s);
			gsl_matrix_set(R,j,i,0); //triangularity
		}
	}
}	

void qr_back(gsl_matrix *R, gsl_vector *qb) //backward substitution
{
	int n=R->size1;
	for(int i=n-1;i>=0;i--)
	{
		double s=0;
		for(int k=i+1;k<n;k++)
			s+=gsl_matrix_get(R,i,k)*gsl_vector_get(qb,k);
		gsl_vector_set(qb,i,(gsl_vector_get(qb,i)-s)/gsl_matrix_get(R,i,i)); //destroys qb and puts solutions
	}
}

void qr_solve(gsl_matrix *Q, gsl_matrix *R, gsl_vector *b, gsl_vector *qb)
{
	gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,qb); // qb = 1*Q^T*b + 0*qb
	qr_back(R,qb); 
}

void qr_inv(gsl_matrix *Q, gsl_matrix* R, gsl_matrix *B) //B inverse of R for proof
{
	int n=R->size1;
	gsl_vector *b = gsl_vector_calloc(n);
	gsl_vector *qb = gsl_vector_calloc(n);
	for(int i=0;i<n;i++)
	{
		gsl_vector_set(b,i,1.0); //Vectors of identity matrix
		qr_solve(Q,R,b,qb);
		gsl_vector_set(b,i,0.0);
		gsl_matrix_set_col(B,i,qb);
	}
	gsl_vector_free(b);
	gsl_vector_free(qb);

}
