#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"
void qr_dec  (gsl_matrix* A, gsl_matrix* R);
void qr_back  (gsl_matrix* A, gsl_vector* R);
void qr_solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b,gsl_vector* qb);
void qr_inv  (gsl_matrix* Q, gsl_matrix* R, gsl_matrix *B);

void printm(gsl_matrix *A)
{
	for(int n=0;n<A->size1;n++)
	{
		for(int m=0;m<A->size2;m++) printf(FMT,gsl_matrix_get(A,n,m));
		printf("\n");
	}
}

int main(int argc, char** argv)
{
	size_t n=4, m=3; //unsinged int
	if(argc>1)n=atoi(argv[1]);
	if(argc>2)m=atoi(argv[2]);
	if(n<7)
	{
		gsl_matrix *A = gsl_matrix_calloc(n,m);
		gsl_matrix *R = gsl_matrix_calloc(m,m);
		for(int i=0;i<n;i++) 
			for(int j=0;j<m;j++) 
				gsl_matrix_set(A,i,j,RND);
		
		printf("QR decomposition:\n");
		printf("\nExercise A.1:\n");
		printf("Some random matrix A:\n"); printm(A);
		
		qr_dec(A,R);
		
		printf("Matrix Q:\n"); printm(A);
		printf("Matrix R:\n"); printm(R);

		gsl_matrix *qtq = gsl_matrix_calloc(m,m);
		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,qtq);
		printf("Matrix Q^TQ (should be 1):\n"); printm(qtq);
		
		gsl_matrix *qr = gsl_matrix_calloc(n,m);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,qr);
		printf("Matrix QR (should be equal A):\n"); printm(qr);
		printf("\nLinear System: \n");
		
		m=4;
		gsl_matrix *AA = gsl_matrix_calloc(m,m);
		gsl_matrix *QQ = gsl_matrix_alloc(m,m);
		for(int i=0;i<m;i++)
			for(int j=0;j<m;j++)
				gsl_matrix_set(AA,i,j,RND);
		
		gsl_matrix_memcpy(QQ,AA);
		printf("\nExercise A.2:\n");
		printf("A square random matrix A:\n"); printm(QQ);
		
		gsl_vector *b = gsl_vector_alloc(m);
		for(int i=0;i<m;i++)
			gsl_vector_set(b,i,RND);
		printf("A random right-hand side b:\n");
		gsl_vector_fprintf(stdout,b,FMT);
		gsl_matrix *RR = gsl_matrix_calloc(m,m);
		qr_dec(QQ,RR);
		
		gsl_vector *qb = gsl_vector_alloc(m);
		qr_solve(QQ,RR,b,qb);
		printf("Solution x:\n"); 
		gsl_vector_fprintf(stdout,qb,FMT);
		gsl_blas_dgemv(CblasNoTrans,1.0,AA,qb,0.0,b);
		printf("Ax, should be equal b:\n"); gsl_vector_fprintf(stdout,b,FMT);
		
		printf("\nExercise B:\n");
		printf("Matrix Q:\n"); printm(QQ);
		printf("Matrix R:\n"); printm(RR);
		gsl_matrix *AI = gsl_matrix_calloc(m,m);
		qr_inv(QQ,RR,AI);
		printf("Inverse matrix A^{-1}:\n"); printm(AI);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,AA,AI,0.0,RR);
		printf("Matrix AA^-1, should be 1:\n"); printm(RR);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,AI,AA,0.0,RR);
		printf("Matrix A^-1A, should be 1:\n"); printm(RR);
		printf("\nExercise C:\n");
	}
	else
	{
		printf("n=%li\n",n);
		gsl_matrix* A=gsl_matrix_alloc(n,n);
		gsl_matrix* R=gsl_matrix_alloc(n,n);
		for(int i=0;i<n;i++)for(int j=0;j<n;j++)gsl_matrix_set(A,i,j,RND);
		qr_dec(A,R);
		gsl_matrix_free(A);
	}
}
