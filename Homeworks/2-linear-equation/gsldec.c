#include <stdio.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>

#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"

int main(int argc, char** argv)
{
	size_t n;
	if(argc>1)n=atoi(argv[1]);
	printf("n=%li\n",n);
	gsl_matrix* A=gsl_matrix_alloc(n,n);
	gsl_vector* R=gsl_vector_alloc(n);
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			gsl_matrix_set(A,i,j,RND);
	gsl_linalg_QR_decomp(A,R);
	gsl_matrix_free(A);
	gsl_vector_free(R);
}
