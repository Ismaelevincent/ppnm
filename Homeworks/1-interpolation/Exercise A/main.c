#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include <math.h>
#include <gsl/gsl_interp.h>
#define GET gsl_vector_get
#ifndef NDEBUG
#define TRACE(args...) fprintf(stderr,args)
#else
#define TRACE(...)
#endif

double linterp(int,double*, double*, double);
double integral(int,double*, double*, double);

int main () 
{
	int n=9;
	double a=-2,b=2,x[n],y[n];

	for(int i=0;i<n;i++)
	{
		x[i]=a+(b-a)*i/(n-1);
		y[i]=1/(1+pow(x[i],2));
	}
	
	gsl_interp* linear = gsl_interp_alloc(gsl_interp_linear,n);
	gsl_interp_init(linear,x,y,n);
	printf("# index 0: data\n");
	int i=0;
	for(double z=x[0];z<=x[n-1];z+=1./16)
	{
		double interp_l=gsl_interp_eval(linear,x,y,z,NULL);
		if(z==x[i])
		{
			printf("%g %g %g %g\n",z,interp_l, linterp(n,x,y,z), y[i]);
			i++;
		}
		else
			printf("%g %g %g\n",z,interp_l, linterp(n,x,y,z));
	}
	printf("\n\n");

	printf("# index 1: integral\n");

	i=0;
	double offset=0;
	for(double z=x[0];z<=x[n-1];z+=1./16)
	{
		double interp_l=gsl_interp_eval_integ(linear,x,y,x[0],z,NULL);
		
		if(z==x[i])
		{
			printf("%g %g %g %g\n",z,interp_l, integral(n,x,y,z)+offset, atan(x[i])-atan(x[0]));
			i++;
			offset+=integral(n,x,y,z);
		}
		else
			printf("%g %g %g\n",z,interp_l, integral(n,x,y,z)+offset);
	}
	printf("\n\n");
	gsl_interp_free(linear);
	return 0;
	
}
