#include<assert.h>
#include<math.h>
#include "qspline.h"

double derivative(int n, double *x, double *y, qspline *s, double z)
{
	assert(n>1 && z>=x[0] && z<=x[n-1]);
	int i=0, j=n-1;
	while(j-i>1) {int m=(i+j)/2; if(z>x[m]) i=m; else j=m;}
        assert(x[i+1]>x[i]);
	return s->b[i]+s->c[i]*(z-x[i])*2;
}
