#include<assert.h>
#include<math.h>
#include "qspline.h"

double integral(int n, double *x, double *y, qspline *s, double z)
{
	assert(n>1 && z>=x[0] && z<=x[n-1]);
	int i=0, j=n-1;
	while(j-i>1) {int m=(i+j)/2; if(z>x[m]) i=m; else j=m;}
        assert(x[i+1]>x[i]);
	return y[i]*(z-x[i])+s->b[i]*pow((z-x[i]),2)/2+s->c[i]*pow((z-x[i]),3)/3;
}
