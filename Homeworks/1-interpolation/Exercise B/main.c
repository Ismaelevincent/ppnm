#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include <math.h>
#include <gsl/gsl_interp.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_spline.h>
#include"qspline.h"
#define GET gsl_vector_get
#ifndef NDEBUG
#define TRACE(args...) fprintf(stderr,args)
#else
#define TRACE(...)
#endif

double linterp(int,double*, double*, double);
double integral(int,double*, double*, qspline*, double);
double derivative(int,double*, double*, qspline*, double);

int main () 
{
	int n=9;
	double a=-2,b=2,x[n],y[n];

	for(int i=0;i<n;i++)
	{
		x[i]=a+(b-a)*i/(n-1);
		y[i]=1/(1+pow(x[i],2));
	}
	gsl_interp_accel *acc = gsl_interp_accel_alloc();
	gsl_spline *spline = gsl_spline_alloc(gsl_interp_cspline,n);
	gsl_spline_init(spline,x,y,n);
	qspline* Q=qspline_alloc(n,x,y);
	printf("# index 0: data\n");
	int i=0;
	for(double z=x[0];z<=x[n-1];z+=1./16)
	{
		double interp_s=gsl_spline_eval(spline,z,NULL);
		if(z==x[i])
		{
			printf("%g %g %g %g\n",z, qspline_eval(Q,z), interp_s, y[i]);
			i++;
		}
		else
			printf("%g %g %g\n",z, qspline_eval(Q,z), interp_s);
	}
	printf("\n\n");

	printf("# index 1: integral\n");

	i=0;
	double offset=0;
	for(double z=x[0];z<=x[n-1];z+=1./16)
	{
		double interp_s=gsl_spline_eval_integ(spline,x[0],z,NULL);
		
		if(z==x[i])
		{
			printf("%g %g %g %g\n",z, integral(n,x,y,Q,z)+offset, interp_s, atan(x[i])-atan(x[0]));
			i++;
			offset+=integral(n,x,y,Q,z);
		}
		else
			printf("%g %g %g\n",z, integral(n,x,y,Q,z)+offset, interp_s);
	}
	printf("# index 2: derivative\n");

	printf("\n\n");
	i=0;
	for(double z=x[0];z<=x[n-1];z+=1./16)
	{
		double interp_s=gsl_spline_eval_deriv(spline,z,NULL);
		if(z==x[i])
		{
			double d=-2*z/pow(1+z*z,2);
			printf("%g %g %g %g\n",z, derivative(n,x,y,Q,z), interp_s, d);
			i++;
		}
		else
			printf("%g %g %g\n",z, derivative(n,x,y,Q,z), interp_s);
	}
	printf("\n\n");
	qspline_free(Q);
	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);
	return 0;	
}
