#include<math.h>
#include<assert.h>
#include<stdio.h>
#include"adapt.h"
#define SQR2 1.41421356237309504880

double open9(double f(double),double a, double b, double acc, double eps, double f2, double f5, double f7, int limit)
{
	double f1=f(a+(b-a)/12.), f3=f(a+3.*(b-a)/12.), f4=f(a+5.*(b-a)/12.), f6=f(a+7.*(b-a)/12.), f8=f(a+10.*(b-a)/12.),f9=f(a+11.*(b-a)/12.);
	double sum=f1+f2+f3+f4+f5+f6+f7+f8+f9;
	double Q=(f1+sum+f9)/11*(b-a), q=sum/9*(b-a);
	double tolerance=acc+eps*fabs(Q), error=fabs(Q-q)/3;
	if(limit==0)
	{
		fprintf(stderr,"open9: recursion limit reached\n");
		return Q;
	}
	if(error < tolerance) return Q;
	else 
	{
		double Q1=open9(f,a,(b+2*a)/3,acc/SQR2,eps,f1,f2,f3,limit-1);
		double Q2=open9(f,(b+2*a)/3,(2*b+a)/3,acc/SQR2,eps,f4,f5,f6, limit-1);
		double Q3=open9(f,(2*b+a)/3,b,acc/SQR2,eps,f7,f8,f9, limit-1);	
		return Q1+Q2+Q3; 
	}
}

double adapt3(double f(double),double a,double b,double acc,double eps)
{	
	double f2=f(a+2.*(b-a)/12.), f5=f(a+6.*(b-a)/12), f7=f(a+9.*(b-a)/12);
	int limit=99;
	return open9(f,a,b,acc,eps,f2,f5,f7,limit);
}

double open4 (double f(double),double a, double b, double acc, double eps, double f2, double f3, int limit)
{
	double f1,f4;
	f1=f(a+(b-a)/6), f4=f(a+5*(b-a)/6);
	double Q=(2*f1+f2+f3+2*f4)/6*(b-a), q=(f1+f4+f2+f3)/4*(b-a);
	double tolerance=acc+eps*fabs(Q), error=fabs(Q-q)/3;
	if(limit==0){
		fprintf(stderr,"open4: recursion limit reached\n");
		return Q;
		}
	if(error < tolerance) return Q;
	else {
		double Q1=open4(f,a,(a+b)/2,acc/SQR2,eps,f1,f2,limit-1);
		double Q2=open4(f,(a+b)/2,b,acc/SQR2,eps,f3,f4,limit-1);
		return Q1+Q2; }
}

double adapt(double f(double),double a,double b,double acc,double eps)
{	
	double f2=f(a+2*(b-a)/6),f3=f(a+4*(b-a)/6);
	int limit=99;
	return open4(f,a,b,acc,eps,f2,f3,limit);
}
