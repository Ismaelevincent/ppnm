My student number ends with 60, therefore my examination project is number 16:
"Adaptive recursive integrator with subdivision into three subintervals"

I've used 9 points for the routine (open9), and I compared it with the one from the homework (open4) for all the suggested integrals and a gsl one as well.
The structure of the program is the same as the homework, in the file "adapt.c" now there are 2 new functions adapt3 (for the three intervals) and open9 (9 points). 
