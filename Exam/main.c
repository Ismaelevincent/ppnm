#include<math.h>
#include<assert.h>
#include<stdio.h>
#include"adapt.h"
#include<gsl/gsl_integration.h>
#define SQR2 1.41421356237309504880

int calls;

int main() 
{

	double f0(double x){calls++; return sqrt(x);}; 
	double a=0,b=1,acc=0.001,eps=0.001;
	calls=0;
	double Q=adapt(f0,a,b,acc,eps);
	double exact=2./3.;
	printf("\n∫ sqrt(x) from %g to %g : OPEN4\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	calls=0;
	Q=adapt3(f0,a,b,acc,eps);
	exact=2./3.;
	printf("\n∫ sqrt(x) from %g to %g : OPEN9\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));


	double f1(double x){calls++; return 4*sqrt(1-x*x)/M_PI;}; 
	a=0,b=1,acc=0.001,eps=0.001;
	calls=0;
	Q=adapt(f1,a,b,acc,eps);
	exact=1;
	printf("\n∫ 4*sqrt(1-x*x)/M_PI from %g to %g : OPEN4\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));
	
	calls=0;
	Q=adapt3(f1,a,b,acc,eps);
	exact=1;
	printf("\n∫ 4*sqrt(1-x*x)/M_PI from %g to %g : OPEN4\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));
	
	double f2(double x){calls++; return 1/sqrt(x);}; 
	a=0,b=1,acc=0.001,eps=0.001;
	calls=0;
	Q=adapt(f2,a,b,acc,eps);
	exact=2;
	printf("\n∫ 1/sqrt(x) from %g to %g : OPEN4\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	calls=0;
	Q=adapt3(f2,a,b,acc,eps);
	exact=2;
	printf("\n∫ 1/sqrt(x) from %g to %g : OPEN9\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));
	a=0,b=1,acc=0.001,eps=0.001;
	
	double f3(double x){calls++; return log(x)/sqrt(x);}; 

	calls=0;
	Q=adapt(f3,a,b,acc,eps);
	exact=-4;
	printf("\n∫ log(x)/sqrt(x) from %g to %g : OPEN4\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	calls=0;
	Q=adapt3(f3,a,b,acc,eps);
	exact=-4;
	printf("\n∫ log(x)/sqrt(x) from %g to %g : OPEN9\n",a,b);
	printf("              Q = %g\n",Q);
	printf("          exact = %g\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	a=0,b=1,acc=1e-9,eps=1e-9;

	calls=0; Q=adapt3(f1,a,b,acc,eps);
	exact=1;
	printf("\n∫ 4*sqrt(1-x*x)/M_PI from %g to %g : OPEN9\n",a,b);
	printf("              Q = %25.20f\n",Q);
	printf("          exact = %25.20f\n",exact);
	printf("          calls = %d\n",calls);
	printf("estimated error = %g\n",acc+fabs(Q)*eps);
	printf("   actual error = %g\n",fabs(Q-exact));

	gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);

	
	double f_gsl(double x, void * params)
	{
		calls++; 
		(void)(params); //avoids warnings
		return 4*sqrt(1-x*x)/M_PI;
	};
	gsl_function F;
	F.function =&f_gsl;
	F.params = 0;
		
	double result, error;
	double expected = 1.0;
	
	gsl_integration_qags (&F, 0, 1, 0, 1e-9, 1000, w, &result, &error);
	
	printf ("\nIntegration with GSL:\n");
	printf ("result          = %25.20f\n", result);
	printf ("exact result    = %25.20f\n", expected);
	printf ("calls           = %d\n",calls);
	printf ("estimated error = %g\n", error);
	printf ("actual error    = %g\n", result - expected);

	gsl_integration_workspace_free (w);

return 0 ;
}
