#include<stdio.h>
#include<limits.h>
#include<float.h>

void tau(double a, double b);

int main()
{
	printf("1.i)\n");
	
	//integer max
	int i=INT_MAX-1000; while(i+1>i) {i++;}
	printf("my max int using while = %i\n",i);

	int c; for(c=INT_MAX-1000; c+1>c; c++){} 
	printf("my max int using for = %i\n",c);

	int u=INT_MAX-1000;
	do{
	u++;
	}while(u+1>u);
	printf("my max int using do = %i\n",u);

	printf("Using INT_MAX = %i \n\n",INT_MAX);



	printf("1.ii)\n");
	
	//integer min
	int j=INT_MIN+1000; while(j-1<j) {j--;}
	printf("my min int using while = %i\n",j);
		
	int d; for(d=INT_MIN+1000; d-1<d; d--){}
	printf("my min int using for = %i\n",d);

	int v=INT_MIN+1000;
	do{
	v--;
	}while(v-1<v);
	printf("my min int using do = %i\n",v);

	printf("Using INT_MIN = %i\n\n",INT_MIN);
	


	printf("1.iii)\n");
	
	//double epsilon
	double k=1; 
	while(1+k!=1){k/=2;} k*=2;
	printf("Double epsilon with while = %g\n",k);

	double e; for(e=1; 1+e!=1; e/=2){} e*=2;
	printf("Double epsilon with for = %g\n",e);

	double x=1;	
	do{
	x/=2;
	}while(1+x!=1); x*=2;
	printf("Double epsilon with do = %g\n",x);

	printf("DBL_EPSILON = %g\n\n",DBL_EPSILON);

	//float epsilon
	float l=1;
	while(1+l!=1){l/=2;} l*=2;
	printf("Float epsilon with while = %g\n",l);

	float f; 
	for(f=1; 1+f!=1; f/=2){} f*=2;
	printf("Float epsilon with for = %g\n",f);

	float y=1;
	do{
	y/=2;
	}while(1+y!=1); y*=2;
	printf("Float epsilon with do = %g\n",y);

	printf("FLT_EPSILON = %g\n\n",FLT_EPSILON);

	//long double
	long double m=1;
	while(1+m!=1){m/=2;} m*=2;
	printf("Long double epsilon with while = %Lg\n",m);

	long double g;
	for(g=1; 1+g!=1; g/=2){} g*=2;
	printf("Long double epsilon with for = %Lg\n",g);

	long double z=1;
	do{
	z/=2;
	}while(1+z!=1); z*=2;
	printf("Long double epsilon with do = %Lg\n",z);
	
	printf("LDBL_EPSILON = %Lg\n\n",LDBL_EPSILON);

	printf("2.i)\n");
	//float_sum
	int max=(int)INT_MAX/300;

	float w=1;
	float sum=0;
	while(w<max){sum+=1.0f/w;w++;}
	printf("Harmonic sum from 1 up to %i in float =%g\n",max,sum);
	//float sum_up_float = 1.0f + 1.0f/2 + 1.0f/3 + ... + 1.0f/max;

	int t=max;
	float sum2=0;
	while(t>0){sum2+=1.0f/t;t--;}
	printf("Harmonic sum from %i down to 1 in float =%g\n\n",max,sum2);


	printf("2.ii)\n");
	printf("In the sum up, at a certain point the smaller values will be neglected because of the limited amount of space of the float type (32 bit). In the sum down, the next number is always bigger, so it's not neglected. \n\n");
	
	printf("2.iii)\n");
	printf("The harmonic series diverges. \n\n");

	printf("2.iv)\n");
	//double sum
	double q=1;
	double sum3=0;
	while(q<max){sum3+=1.0/q;q++;}
	printf("Harmonic sum from 1 to %i in double =%g\n",max,sum3);
	
	double p=max;
	double sum4=0;
	while(p>0){sum4+=1.0/p;p--;}
	printf("Harmonic sum from %i down to 1 in double =%g\n",max,sum4);

	printf("The double type as a much higher tollerance for small numbers (double precision) as it has 64 bit of memory. \n\n");
	tau(1.,0.5);

return 0;
}

