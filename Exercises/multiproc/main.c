#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<pthread.h>

struct params {long int N, N_in; unsigned int s;};
struct vec {double x,y;};

void* pi_est(void* arg)
{
	struct params *p = (struct params*)arg;
	rand_r(&(p->s));
	double x,y;
	for(int i=0; i<p->N; ++i) 
	{
		x = (double)rand_r(&(p->s))/RAND_MAX;
		y = (double)rand_r(&(p->s))/RAND_MAX;
		if (pow(x,2)+pow(y,2)<=1)
			p->N_in+=1;
	}

	return NULL;
}

int main(int argc, char* argv[]) //it allows the choice of the number of processors
{
	int N_proc=4;
	long int Ntot=100;
	if(argc > 1 && argv[2]!="" && atoi(argv[2])<8)
	{
		N_proc = atoi(argv[2]);
		Ntot = (long int)atof(argv[1]);
	}
	srand(time(NULL));

	pthread_t t[N_proc];
	struct params p[N_proc];
	for(int i=0; i<N_proc; i++)	
	{	
		p[i] =(struct params) {.N=Ntot/N_proc, .N_in=0, .s=rand()};
	}
//	
        for(int i=0; i<N_proc; i++)
	{
		pthread_create(&t[i], NULL, pi_est, (void*)&p[i]);
	}
//        
	for(int i=0; i<N_proc; i++)
		pthread_join(t[i], NULL);
//	
	unsigned int N_in=0;
        for(int i=0; i<N_proc; i++)
		N_in+=p[i].N_in; 

	double pi = 4.0*N_in/Ntot;
	printf("pi=%1.18g\nacos(-1)= %1.18g\n", pi, acos(-1));

	return 0;
}
	
