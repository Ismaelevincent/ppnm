#ifndef HAVE_VECTOR_H /* for multiple includes */
#define HAVE_VECTOR_H

typedef struct {int size; double* data;} vector;

vector* vector_alloc       (int n);    
void    vector_free        (vector* v);                      
void    vector_set         (vector* v, int i, double value); 
double  vector_get         (vector* v, int i);       
double  vector_dot_product (vector* u, vector* v);   
#endif
