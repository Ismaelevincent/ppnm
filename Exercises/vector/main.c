#include "vector.h"
#include "stdio.h"
#include"assert.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

	printf("\nmain: testing vector_alloc ...\n");
	vector *v = vector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing vector_set and vector_get ...\n");
	double value = RND;
	int i = n / 2;
	vector_set(v, i, value);
	double vi = vector_get(v, i);
	if (vi==value) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing vector_dot_product (orthonormal vectors) ...\n");
	vector *a = vector_alloc(n);
	vector *b = vector_alloc(n);
	for (int i = 0; i < n; i++) 
	{
		vector_set(a, i, 0);
		vector_set(b, i, 0);
	}

	vector_set(a, 1, 1);
	vector_set(b, 2, 1);
	value=vector_dot_product(a,b);
	printf("(a,b) should   = 0\n");
	printf("(a,b) actually = %g\n", value);
	value=vector_dot_product(a,a);
	printf("(a,a) should   = 1\n");
	printf("(a,a) actually = %g\n", value);
	vector_free(v);
	vector_free(a);
	vector_free(b);

	return 0;
}
