#include<stdio.h>
#include<math.h>
int main(int arc, char** argv)
{	
	double x;
	FILE* my_in_stream = fopen(argv[1],"r");
	FILE* my_out_stream = fopen(argv[2], "w");
	int items;
	fprintf(my_out_stream,"x sin(x) cos(x)\n"); 
	do {
		items = fscanf(my_in_stream,"%lg", &x);
		fprintf(my_out_stream,  "%g %g %g\n", x, sin(x),cos(x)); 
	}while(items!=EOF);
	fclose(my_in_stream);
	fclose(my_out_stream);
return 0;
}
