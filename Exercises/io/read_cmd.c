#include<stdlib.h>
#include<stdio.h>
#include<math.h>

int main(int argc, char** argv){
	if(argc<2) fprintf(stderr,"%s: there were no arguments\n",argv[0]);
	else 
	{
		printf("x sin(x) cos(x) \n");
		for(int i=1;i<argc;i++)
		{
			double x = atof(argv[i]);
			printf("%g %g %g\n",x, sin(x), cos(x));
		}
	}
return 0;
}
