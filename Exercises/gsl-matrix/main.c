#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_linalg.h>

#ifndef NDEBUG
#define TRACE(args...) fprintf(stderr,args)
#else
#define TRACE(...)
#endif

#define n 3

void vector_print(char s[], gsl_vector* v)
{
	printf("%s\n",s);
	for(int i=0;i<v->size;i++)printf("%10g ",gsl_vector_get(v,i));
	printf("\n\n");
}

void matrix_print(char s[], gsl_matrix* M)
{
	printf("%s\n",s);
	for(int i=0; i< M->size1; i++)
	{
		for(int j=0; j<M->size2; j++)
		{
			printf("%10g ",gsl_matrix_get(M,i,j));
		}
		printf("\n");
	}
	printf("\n\n");
}


int main()
{
	int err=1;
	TRACE("%i\n",err);
	err+=1;

	gsl_matrix* A=gsl_matrix_alloc(n,n);
	gsl_matrix* Acopy=gsl_matrix_alloc(n,n);
	gsl_vector* x=gsl_vector_alloc(n);
	gsl_vector* y=gsl_vector_alloc(n);
	gsl_vector* yproof=gsl_vector_alloc(n);

	TRACE("%i\n",err);
	err+=1;
	double matrix[n][n]= {
		{6.13, -2.90, 5.86}, 
		{8.08, -6.31, -3.89},
		{-4.36, 1.00, 0.19}};

	TRACE("%i\n",err);
	err+=1;
	double vector[n]= {6.23, 5.37, 2.29};

	for(int i=0; i< y->size; i++)
		{
		gsl_vector_set(y,i,vector[i]);
		}
	
	for(int i=0; i< A->size1; i++)
		for(int j=0; j<A->size2; j++)
		{
		gsl_matrix_set(A,i,j,matrix[i][j]);
		}
	gsl_matrix_memcpy(Acopy, A);
	gsl_linalg_HH_solve(A, y, x);

	TRACE("%i\n",err);
	err+=1;
	matrix_print("Matrix A is", Acopy);
	vector_print("Vector y is", y);
	vector_print("Solution x is", x);
	TRACE("%i\n",err);
	err+=1;
	
	gsl_blas_dgemv (CblasNoTrans, 1.0, Acopy, x,
                  0.0, yproof);
	vector_print("A*x =", yproof);

	return 0;

}
