#include<stdio.h>
#include<math.h>
#include<gsl/gsl_sf.h>
double Gamma(double x)
{
	if(x<0)return M_PI/sin(M_PI*x)/Gamma(1-x);
	if(x<9)return Gamma(x+1)/x;
	double lnGamma=x*log(x+1/(12*x-1/x/10))-x+log(2*M_PI/x)/2;
	return exp(lnGamma);
}

int main()
{
	double xmin=1.0/8,xmax=5;
	for(double x=xmin;x<=xmax;x+=1.0/8)
	{
			printf("%lg %lg %lg %lg\n", x,Gamma(x), tgamma(x), gsl_sf_gamma(x));
	}
	
return 0;
}
