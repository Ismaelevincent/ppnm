#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6

int main()
{
	komplex a = {1,2}, b = {3,4};

	printf("testing komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should be = ", R);
	komplex_print("a+b actually is = ", r);

	printf("testing komplex_sub...\n");
	r.re = komplex_sub(a,b).re;
	r.im = komplex_sub(a,b).im;
	R.re = -2;
	R.im = -2;
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should be = ", R);
	komplex_print("a+b actually is = ", r);


	
return 0;
}
